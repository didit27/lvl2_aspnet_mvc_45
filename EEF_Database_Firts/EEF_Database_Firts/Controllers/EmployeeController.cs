﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Transactions;
using System.Web;
using System.Web.Mvc;

namespace EEF_Database_Firts.Controllers
{
    public class EmployeeController : Controller
    {
        EmployeeContext db = new EmployeeContext();

        //TransactionScope ts = new TransactionScope();
        // GET: Employee
        public ActionResult Index()
        {
            return View(db.Employees.ToList());
        }


        [HttpGet]
        public ActionResult Create()
        {
            return View();
        }
        [HttpPost]
        public ActionResult Create(Employee employee)
        {
            if (ModelState.IsValid)
            {
                using (DbContextTransaction ts = db.Database.BeginTransaction())
                {
                    try
                    {
                        db.Employees.Add(employee);
                        db.SaveChanges();
                        if (employee.id > 0)
                        {
                            //int a = 0;
                            //int total = 10 / a;
                            tbl_salary tbl_Salary1 = new tbl_salary();
                            tbl_Salary1.fk_id_employee = employee.id;
                            tbl_Salary1.Salary = employee.Salary;
                            db.tbl_salary.Add(tbl_Salary1);
                            db.SaveChanges();
                            ts.Commit();

                        }
                        return RedirectToAction("Index");


                    }
                    catch (Exception ex)
                    {
                        //
                        ts.Rollback();
                        ViewData["data"] = ex.Message;

                    }

                }
            }
            return View();

        }

        public ActionResult Details(int id)
        {
            using (EmployeeContext db = new EmployeeContext())
            {
                return View(db.Employees.Where(x => x.id == id).FirstOrDefault());
            }
        }

        public ActionResult Edit(int id)
        {
            //mengambil data
            Employee employee = db.Employees.Where(x => x.id == id).FirstOrDefault();

            return View(employee);
        }
        //mengupload data
        [HttpPost]
        public ActionResult Edit(Employee employee)
        {
            //menangkap error
            using (DbContextTransaction dbts = db.Database.BeginTransaction())
            {
                try
                {
                    //TODO Add update logic here
                    db.Entry(employee).State = EntityState.Modified;
                    db.SaveChanges();
                    if (employee.id > 0)
                    {
                        //int a = 0;
                        //int total = 10 / a;
                        tbl_salary tbl_Salary1 = db.tbl_salary.Where(x => x.fk_id_employee == employee.id).FirstOrDefault();
                        tbl_Salary1.fk_id_employee = employee.id;
                        tbl_Salary1.Salary = employee.Salary;
                        db.Entry(tbl_Salary1).State = EntityState.Modified;
                        db.SaveChanges();
                        dbts.Commit();

                    }
                    return RedirectToAction("Index");
                }
                catch (Exception ex)
                {
                    dbts.Rollback();
                    ViewData["data"] = ex.Message;
                }
                return View();
            }

        }

        public ActionResult Delete(int id)
        {
            //mengambil data
            Employee employee = db.Employees.Where(x => x.id == id).FirstOrDefault();

            return View(employee);
        }
        [HttpPost]
        public ActionResult Delete(int id, FormCollection collection)
        {
            //menangkap error
            using (DbContextTransaction dbts = db.Database.BeginTransaction())
            {
                try
                {
                    //TODO Add update logic here
                    if (id > 0)
                    {
                        //int a = 0;
                        //int total = 10 / a;
                        tbl_salary tbl_Salary1 = db.tbl_salary.Where(x => x.fk_id_employee == id).FirstOrDefault();
                        db.tbl_salary.Remove(tbl_Salary1);
                        db.SaveChanges();
                    }

                    Employee e = db.Employees.Where(x => x.id == id).FirstOrDefault();
                    db.Employees.Remove(e);
                    db.SaveChanges();
       
                    dbts.Commit();
                    return RedirectToAction("Index");
    
                }
                catch (Exception e)
                {
                    dbts.Rollback();
                    ViewData["data"] = e.Message;
                }
                return View();
            }
        }
    }
}