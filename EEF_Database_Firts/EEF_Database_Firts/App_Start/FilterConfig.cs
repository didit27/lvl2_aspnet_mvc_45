﻿using System.Web;
using System.Web.Mvc;

namespace EEF_Database_Firts
{
    public class FilterConfig
    {
        public static void RegisterGlobalFilters(GlobalFilterCollection filters)
        {
            filters.Add(new HandleErrorAttribute());
        }
    }
}
