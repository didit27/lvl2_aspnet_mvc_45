
-- --------------------------------------------------
-- Entity Designer DDL Script for SQL Server 2005, 2008, 2012 and Azure
-- --------------------------------------------------
-- Date Created: 04/21/2020 13:57:58
-- Generated from EDMX file: C:\NawaData\Git\LVL2_ASPNet_MVC_45\EEF_Database_Firts\EEF_Database_Firts\EmployeeModel.edmx
-- --------------------------------------------------

SET QUOTED_IDENTIFIER OFF;
GO
USE [HR14];
GO
IF SCHEMA_ID(N'dbo') IS NULL EXECUTE(N'CREATE SCHEMA [dbo]');
GO

-- --------------------------------------------------
-- Dropping existing FOREIGN KEY constraints
-- --------------------------------------------------


-- --------------------------------------------------
-- Dropping existing tables
-- --------------------------------------------------

IF OBJECT_ID(N'[dbo].[Employee]', 'U') IS NOT NULL
    DROP TABLE [dbo].[Employee];
GO
IF OBJECT_ID(N'[dbo].[tbl_salary]', 'U') IS NOT NULL
    DROP TABLE [dbo].[tbl_salary];
GO

-- --------------------------------------------------
-- Creating all tables
-- --------------------------------------------------

-- Creating table 'Employees'
CREATE TABLE [dbo].[Employees] (
    [id] int  NOT NULL,
    [FirstName] nvarchar(50)  NOT NULL,
    [LastName] nvarchar(50)  NOT NULL,
    [MiddleName] nvarchar(50)  NULL,
    [Address] nvarchar(300)  NULL,
    [Email] nvarchar(50)  NULL
);
GO

-- Creating table 'tbl_salary'
CREATE TABLE [dbo].[tbl_salary] (
    [pk_id] int IDENTITY(1,1) NOT NULL,
    [Name] varchar(50)  NULL,
    [Salary] int  NULL
);
GO

-- --------------------------------------------------
-- Creating all PRIMARY KEY constraints
-- --------------------------------------------------

-- Creating primary key on [id] in table 'Employees'
ALTER TABLE [dbo].[Employees]
ADD CONSTRAINT [PK_Employees]
    PRIMARY KEY CLUSTERED ([id] ASC);
GO

-- Creating primary key on [pk_id] in table 'tbl_salary'
ALTER TABLE [dbo].[tbl_salary]
ADD CONSTRAINT [PK_tbl_salary]
    PRIMARY KEY CLUSTERED ([pk_id] ASC);
GO

-- --------------------------------------------------
-- Creating all FOREIGN KEY constraints
-- --------------------------------------------------

-- --------------------------------------------------
-- Script has ended
-- --------------------------------------------------